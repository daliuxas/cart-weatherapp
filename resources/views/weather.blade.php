@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Weather app <a href="/settings" class="float-right btn
                    btn-sm btn-primary">Settings</a></div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Selected city: <span class="float-right">{{\App\City::find(1)
                        ->city_name}}</span></li>
                        <li class="list-group-item">Temperature: <span class="float-right">{{$data["temp"]}}C</span></li>
                        <li class="list-group-item">Wind speed: <span
                                    class="float-right">{{$data["wind_speed"]}}m/s</span></li>
                        <li class="list-group-item">Wind direction: <span
                                    class="float-right">{{$data["wind_dir"]}}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
