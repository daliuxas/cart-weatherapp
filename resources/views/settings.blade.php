@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Weather app Settings <a href="/weather" class="btn btn-sm float-right
                    btn-secondary">Return</a>
                    </div>
                    <div class="card-body">
                        <form action="{{route('updateSettings')}}" enctype="multipart/form-data" method="post">
                            @csrf
                            {{ method_field('PATCH') }}
                            <div class="form-group">
                                <label for="cityName">City</label>
                                <input type="text" class="form-control" id="cityName" name="cityName" value="{{\App\City::find(1)
                        ->city_name}}">
                            </div>
                            <div class="form-group">
                                <label for="notificationEmail">Notification email</label>
                                <input type="text" class="form-control" id="notificationEmail"
                                       name="notificationEmail" value="{{Auth::user()->notification_email}}">
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="notification" name="notification"
                                       @if(Auth::user()->notification==1)
                                           checked
                                           @endif
                                >
                                <label class="form-check-label" for="notification">Receive email notifications</label>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
