@extends('layouts.app')

@section('content')

    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Weather App</h1>
            <p>
                @if (Route::has('login'))
                    @auth
                        <a href="{{ url('/weather') }}" class="btn btn-primary my-2">Weather App</a>
                    @else
                        <a href="{{ route('login') }}" class="btn btn-primary my-2">Login</a>
                        <a href="{{ route('register') }}" class="btn btn-secondary my-2">Register</a>
                    @endauth
                @endif

            </p>
        </div>
    </section>
@endsection