<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\weatherNotification;
use Illuminate\Support\Facades\Cache;
use App\User;
use App\City;

class checkWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkWeather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check wind speeds and send emails to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $city          = City::find(1);
        $city_name     = $city->city_name;
        $request       = 'http://api.openweathermap.org/data/2.5/weather?q=' . $city_name . '&APPID=198157189b481b757d77bfbaeac86902';
        $response      = file_get_contents($request);
        $json_response = json_decode($response);
        $wind_speed    = $json_response->wind->speed;

        $cached_wind_speed = Cache::get('wind_speed');
        if ( ! $cached_wind_speed) {
            Cache::add('wind_speed', $wind_speed, 10000);
        }

        $users = User::where('notification', true)->get();

        if ($users && $cached_wind_speed) {
            if (($wind_speed > 10 && $cached_wind_speed <= 10) || ($wind_speed < 10 && $cached_wind_speed >= 10)) {
                foreach ($users as $user) {
                    Mail::send('emails.weatherNotification', ["wind_speed" => $wind_speed], function ($m) use ($user) {
                        $m->from('weather@app.com', 'WeatherApp');
                        $m->to($user->notification_email, $user->name)->subject('Your wind notification!');
                    });
                }
            }
        }

        Cache::store('file')->put('wind_speed', $wind_speed, 10000);
    }
}
