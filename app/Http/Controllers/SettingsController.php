<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use App\User;

class SettingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('settings');
    }

    public function updateSettings(Request $request)
    {
        $this->validate($request, [
            'cityName'          => 'required|string',
            'notificationEmail' => 'required|string|email|max:255',
        ]);

        $city            = City::find(1);
        $city->city_name = $request->input('cityName');
        $city->save();

        $user                     = User::find(auth()->user()->id);
        $user->notification       = $request->input('notification')? true : false;
        $user->notification_email = $request->input('notificationEmail') ;
        $user->save();

        return redirect('/settings')->with('success', 'Settings updated');
    }
}
