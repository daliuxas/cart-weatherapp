<?php

namespace App\Http\Controllers;

use App\Mail\weatherNotification;
use Illuminate\Http\Request;
use App\City;
use Illuminate\Support\Facades\Mail;

class WeatherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $city          = City::find(1);
        $city_name     = $city->city_name;
        $request       = 'http://api.openweathermap.org/data/2.5/weather?q=' . $city_name . '&APPID=198157189b481b757d77bfbaeac86902';
        $response      = file_get_contents($request);
        $json_response = json_decode($response);

        $temp       = $json_response->main->temp - 273.15;
        $wind_speed = $json_response->wind->speed;
        $wind_dir   = "Almost no wind";

        if (property_exists($json_response->wind, "deg")) {
            $wind_dir_raw = $json_response->wind->deg;
            if ($wind_dir_raw > 335 && $wind_dir_raw < 25) {
                $wind_dir = "North";
            } elseif ($wind_dir_raw < 65) {
                $wind_dir = "North East";
            } elseif ($wind_dir_raw < 110) {
                $wind_dir = "East";
            } elseif ($wind_dir_raw < 160) {
                $wind_dir = "South East";
            } elseif ($wind_dir_raw < 205) {
                $wind_dir = "South";
            } elseif ($wind_dir_raw < 250) {
                $wind_dir = "South West";
            } elseif ($wind_dir_raw < 290) {
                $wind_dir = "West";
            } elseif ($wind_dir_raw <= 335) {
                $wind_dir = "North West";
            }
        }

        $data = [
            "temp"       => $temp,
            "wind_speed" => $wind_speed,
            "wind_dir"   => $wind_dir,
        ];

        return view('weather')->with('data', $data);
    }
}
