<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //table name
    protected $table='city';

    //primary key
    public $primaryKey='id';

    //timestamps
    public $timestamps = true;
}
