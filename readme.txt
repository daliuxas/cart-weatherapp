- City is used for all users and is stored as a single DB entry in city table

- User email used for notifications set from login email by default, later can be edited in settings.
- User can check whether he wants to receive email notifications.

- settings controller is used to update user data and city.

- weather controller used to request data from API and display it.

- checkWeather.php command is used to check the current wind speed, check whether it has dropped or risen
above 10 according to cached data from previous request, then sends emails to all users who have enabled notifications
and stores the current wind speed value in cache.

- php artisan schedule can be the nset up in a server to run the checkWeather command at wanted interwals.

